const path = require('path'),
  webpack = require('webpack'),
  // HtmlWebpackPlugin = require('html-webpack-plugin'),
  ContextReplacementPlugin = require('webpack').ContextReplacementPlugin;

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, '../dist', 'assets/js'),
    filename: '[name].js',
    publicPath: 'assets/js/'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: "commons",
          chunks: "initial",
          minChunks: 2
        },
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    }
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    }]
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   template: 'src/index.html',
    //   filename: '../../index.html',
    //   showErrors: true,
    //   inject: false,
    //   minify: {
    //     removeComments: true,
    //     collapseWhitespace: true,
    //     removeRedundantAttributes: true,
    //     useShortDoctype: true,
    //     removeEmptyAttributes: true,
    //     removeStyleLinkTypeAttributes: true,
    //     keepClosingSlash: true,
    //     minifyJS: true,
    //     minifyCSS: true,
    //     minifyURLs: true,
    //   }
    // }),
    // new webpack.optimize.CommonsChunkPlugin({
    //     name: "commons",
    //     filename: "commons.js",
    //     minChunks: 3
    // }),
    // new webpack.optimize.CommonsChunkPlugin({
    //     name: "manifest",
    //     minChunks: Infinity
    // })
  ],
  // devtool: 'cheap-module-source-map'
}
