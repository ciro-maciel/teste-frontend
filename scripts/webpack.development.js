const merge = require('webpack-merge');
const common = require('./webpack.common.js'),
  path = require('path'),
  DefinePlugin = require('webpack').DefinePlugin;


module.exports = merge(common, {
  plugins: [
    new DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      }
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    compress: true,
    port: 9000,
    open: true,
    // https://stackoverflow.com/questions/32098076/react-router-cannot-get-except-for-root-url
    // https://webpack.github.io/docs/webpack-dev-server.html
    // https://webpack.js.org/configuration/dev-server/
    historyApiFallback: true
  }
});
