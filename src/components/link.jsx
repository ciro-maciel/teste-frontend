import React, { Component, PureComponent } from 'react';
import styled from 'styled-components';

import { color } from "./variables";


const Container = styled.a`
    color: ${color.primary};
    text-decoration: none;
`;


class Link extends PureComponent {

  render() {

    const { children } = this.props;

    return (
      <Container>
        {children}
      </Container>
    )

  }
}

export default Link;
