

export const color = {
  normal: '#666665',
  primary: "#1c6f41",
  hover: 'black',
  selected: "#004020",
  inverted: "white",
  featured:"#f5b518"
}

export const border = {
  color: "#cccccc"
};


