import React, { Component } from 'react';

import Slider from 'react-styled-carousel';

// https://getbootstrap.com/docs/4.1/layout/overview/#responsive-breakpoints
// https://github.com/ItsMrAkhil/react-styled-carousel
const responsive = [
  { breakPoint: 992, cardsToShow: 3 },
  { breakPoint: 768, cardsToShow: 2 },
  { breakPoint: 576, cardsToShow: 1 }
];

const leftArrow = () => (
  <i style={{ fontSize: "35px", color: "#1c6f41", position: "absolute", left: "0", top: "43%", cursor: "pointer" }} className="fas fa-angle-left"></i>
);
const rightArrow = () => (
  <i style={{ fontSize: "35px", color: "#1c6f41", position: "absolute", top: "43%", cursor: "pointer" }} className="fas fa-angle-right"></i>
);



const Carrossel = (props) => (
  <Slider showDots={false} responsive={responsive} cardsToShow={2} autoSlide={false} infinite={true} style={{ margin: "20px" }} LeftArrow={leftArrow()} RightArrow={rightArrow()}>
    {props.children}
  </Slider>
);

export default Carrossel;
