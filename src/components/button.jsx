import React, { Component, PureComponent } from 'react';
import styled from 'styled-components';

import { color } from "./variables";


const ContainerBase = styled.button`
    background-color: ${color.primary};
    outline: none;
    color: white;
    cursor: pointer;
    padding: 8px 30px;
    border-radius:6px;
    box-shadow: rgba(100, 100, 100, 0.43) 3px 3px 4px;
`;

const ContainerSearch = ContainerBase.extend`
  height: 50px;
  border: 0;
  padding: 10px 30px;
  border-radius: 0px 6px 6px 0px;
`;



class Button extends PureComponent {

  render() {

    const { children } = this.props;

    return (
      <ContainerBase>
        {children}
      </ContainerBase>
    )

  }
}

export default Button;


export class ButtonSearch extends PureComponent {

  render() {

    const { children } = this.props;

    return (
      <ContainerSearch>
        {children}
      </ContainerSearch>
    )

  }
}

