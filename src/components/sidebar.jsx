import React, { Component } from 'react';

import styled from 'styled-components';

import { border } from "./variables";
import { H2 } from "./typography.jsx";
import Link from "./link.jsx";


const ContainerSidebar = styled.div`
  display: flex;
  flex-wrap: wrap;
  border: 1px solid ${border.color};
  border-top: none;
`;

const ContainerItem = styled.div`
  width: 100%;
`;

const HeaderItem = styled.div`
  width: 100%;
  padding: 0 20px 0 20px;
  border-bottom: 1px solid ${border.color};
  border-top: 1px solid ${border.color};
`;

const ContainerMenu = styled.div`
  width: 100%;
`;

const ContainerBody = styled.div`
  width: 100%;
  padding: 0 20px 0 20px;
`;

const ListMenu = styled.ul`
  list-style-type: none;
  padding-left: 0;
  margin-top: 0;
  box-sizing: border-box;
`;


class Menu extends Component {

  orderByName(data) {
    if (data) {
      return data.sort(function (a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
    }
  }

  render() {

    let { name, itens, isLast } = this.props;
    itens = this.orderByName(itens);

    return (
      <ContainerMenu>
        <ContainerBody>
          <H2 style={{ fontWeight: "500" }}>
            {name}
          </H2>
          <ListMenu>
            {itens.map((item, index) =>
              <li key={index}>
                {item.name}&nbsp;(<Link>{item.count}</Link>)
              </li>
            )}
          </ListMenu>
        </ContainerBody>
        {(!isLast)?
          <hr style={{ width: "80%" }} />
          :
          <br/>
        }
      </ContainerMenu>
    )
  }

}

class Item extends Component {

  render() {

    const { title, menus } = this.props;

    return (
      <ContainerItem>
        <HeaderItem>
          <H2>
            {title}
          </H2>
        </HeaderItem>
        {menus.map((menu, index) =>
          <Menu key={index} name={menu.name} itens={menu.itens} isLast={(menus.length - 1) === index} />
        )}
      </ContainerItem>
    )
  }

}

class Sidebar extends Component {

  orderByCategory(data) {
    if (data) {
      return data.sort(function (a, b) { return (a.category > b.category) ? 1 : ((b.category > a.category) ? -1 : 0); });
    }
  }

  render() {

    let items = this.props.data;
    items = this.orderByCategory(items);

    return (
      <div>
        {
          items ?
            <ContainerSidebar>
              {items.map(function (item, index) {
                return <Item key={index} title={item.category} menus={item.menus} />;
              })}
            </ContainerSidebar>
            :
            null
        }
      </div>
    )
  }
}

export default Sidebar;
