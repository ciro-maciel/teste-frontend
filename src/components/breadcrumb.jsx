import React, { Component } from 'react';
import styled from 'styled-components';

import { border } from "./variables";
import { Strong } from "./typography.jsx";


const Container = styled.div`
  height: 40px;
  display: flex;
  width: 100%;
  align-items: center;
  border-bottom:1px solid ${border.color};
  margin-bottom:30px;
`;


class Breadcrumb extends Component {
  render() {
    return (
      <Container>
        Nome da área 	&nbsp; &raquo; &nbsp; Nome da área &nbsp; &raquo; &nbsp; Nome da área	&nbsp; &raquo; &nbsp;
        <Strong primary>
          Nome da área
        </Strong>
      </Container>
    )
  }
}


export default Breadcrumb;
