import React, { Component } from 'react';
import styled from 'styled-components';

import { color } from "./variables";

import Search from "./search.jsx";


const Container = styled.div`
  width: 100%;
  display: flex;
  height: 100px
`;

const ContainerLogo = styled.div`
  width: 150px;
  align-self: center;
`;

const ContainerHeader = styled.div`
  width: 100%;
  align-self: center;
  display: flex;
  padding-top: 20px;
`;

const ContainerSearch = styled.div`
  flex: 1 1 0;
  padding-left: 30px;
  padding-right: 30px;
`;

const ContainerUser = styled.div`
  flex: 1 1 0;
  display: flex;
  align-items: center;
`;

const ContainerAccount = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding-right: 20px;
  border-right: 1px solid #f5b519;
`;

const ContainerCart = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding-left: 20px;
`;

const ImageHeader = styled.i`
  font-size:40px;
  color:#f5b518;
  margin-right: 10px;
`;



class Header extends Component {
  render() {
    return (
      <Container>
        <ContainerLogo>
          <img src="../assets/img/vv-logo.png" alt="" />
        </ContainerLogo>
        <ContainerHeader>
          <ContainerSearch>
            <Search />
          </ContainerSearch>
          <ContainerUser>
            <ContainerAccount>
              <ImageHeader className="fas fa-user-tie" />
              <div>
                <span>
                  Ola! Joao Adolfo
                </span>
                <br />
                <strong>
                  Minha Conta
                  </strong>
              </div>
            </ContainerAccount>
            <ContainerCart>
              <ImageHeader className="fas fa-cart-arrow-down" />
              <div>
                <span>
                  Meu Carrinho
              </span>
                <br />
                <strong>
                  0 itens
                </strong>
              </div>
            </ContainerCart>
          </ContainerUser>
        </ContainerHeader>
      </Container>
    )
  }
}


export default Header;
