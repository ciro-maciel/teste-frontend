import React, { Component } from 'react';
import styled from 'styled-components';

import { color } from "./variables";
import { ButtonSearch } from "./button.jsx";


const ImageSearch = styled.i`
  font-size: 30px;
  color: ${color.primary};
  margin-left: 10px;
  position: absolute;
`;

const InputSearch = styled.input`
  width: 350px;
  height: 50px;
  border-radius: 6px 0px 0px 6px;
  text-indent:50px;
  font-size:18px
`;


class Search extends Component {
  render() {
    return (
      <form style={{ display: "flex", alignItems: "center" }}>
        <ImageSearch className="fas fa-search" />
        <InputSearch type="search" placeholder="Encontre aqui as melhores ofertas" />
        <ButtonSearch>
          <strong style={{ fontSize: "15px" }}>
            Buscar
          </strong>
        </ButtonSearch>
      </form>
    )
  }
}


export default Search;
