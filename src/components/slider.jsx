import React, { Component } from 'react';

import { Flex, Box } from 'grid-styled';

import styled, { keyframes } from 'styled-components';
// https://daneden.github.io/animate.css/

const slideInLeft = keyframes`
  from {
    transform: translate3d(-70%, 0, 0);
  }

  to {
    transform: translate3d(0, 0, 0);
  }
`;

const slideInRight = keyframes`
  from {
    transform: translate3d(70%, 0, 0);
  }

  to {
    transform: translate3d(0, 0, 0);
  }
`;

const MoveDiv = styled.div`
  // border: 1px solid red;
  // margin: 7px;
  flex: 1;
  flexWrap: "wrap";
  width:216px;
  // height: 390px;
  ${props => props.direction === 'left' && `
    animation: 1s ${slideInLeft};
  `}
  ${props => props.direction === 'right' && `
    animation: 1s ${slideInRight};
  `}
`;


class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.children,
      actualPage: 1,
      direction: "-"
    };
  }

  paginate(array, page_size, page_number) {
    --page_number;
    return array.slice(page_number * page_size, (page_number + 1) * page_size);
  }

  moveLeft() {
    let index = this.state.actualPage;

    index = index === 1 ? (this.state.items.length / 3) : index - 1;

    this.setState({ actualPage: index, direction: "-" }, () => {
      setTimeout(() => {
        this.setState({ direction: "left" });
      }, 10);
    });
  }

  moveRight() {
    let index = this.state.actualPage;

    index = index === (this.state.items.length / 3) ? 1 : index + 1;

    this.setState({ actualPage: index, direction: "-" }, () => {
      setTimeout(() => {
        this.setState({ direction: "right" });
      }, 10);
    });
  }

  render() {

    const items = this.paginate(this.state.items, 3, this.state.actualPage),
      { direction } = this.state;

    return (
      <ul style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px", padding: "0", listStyleType: "none" }}>
        <li>
          <div onClick={(e) => {
            this.moveLeft();
          }}
            style={{ width: "30px", height: "390px", alignItems: "center", display: "flex", cursor: "pointer" }}>
              {/* <img src="../assets/img/icons/icon-arrow-left.png" alt="" style={{ width: "30px" }} /> */}
              <i style={{fontSize:"40px", color:"#1c6f41"}} className="fas fa-angle-left"></i>
          </div>

        </li>
        <li style={{ flex: 1, display: "flex", flexWrap: "wrap" }}>
          {items.map((item, index) =>
            <MoveDiv key={index} direction={direction}>
              {item}
            </MoveDiv>
          )}
        </li>
        <li>
          <div onClick={(e) => {
            this.moveRight();
          }}
            style={{ width: "30px", height: "390px", alignItems: "center", display: "flex", justifyContent:"flex-end", cursor: "pointer" }}>
              {/* <img src="../assets/img/icons/icon-arrow-rigth.png" alt="" style={{ width: "30px" }} /> */}
              <i style={{fontSize:"40px", color:"#1c6f41"}} className="fas fa-angle-right"></i>
          </div>
        </li>
      </ul>
    )
  }
}

export default Slider;
