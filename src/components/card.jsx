import React, { Component } from 'react';


class Card extends Component {


  render() {

    const { pictureSrc, info, priceHigh, priceLow, paymentType } = this.props;

    return (
      <div style={{
        height: "390px",
        border: "1px solid #cccccc",
        borderRadius: "6px",
        boxShadow: "3px 3px 10px rgba(100,100,100,.4)",
        margin: "7px",
        boxSizing: "border-box",
        padding: "20px",
        display: "flex",
        flex: 1,
        flexWrap: "wrap",
        justifyContent: "center",
        cursor:"pointer"
      }}>
        <img src={pictureSrc} alt="" style={{ width: "160px", height: "160px" }} />

        <div style={{ width: "100%", height: "25px", textAlign: "center", backgroundColor: "#333333", marginTop: "10px", display: "flex", alignItems: "center", justifyContent: "center" }}>
          <strong style={{ color: "#f5b518" }}>
            Lançamento
          </strong>
        </div>

        <p style={{ textAlign: "justify", textOverflow: "ellipsis", whiteSpace: "nowrap", overflow: "hidden", fontSize: "11px" }}>
          {info}
        </p>

        <hr style={{ width: "100%", border: "1px solid #cccccc", height:"fit-content"}} />

        <span style={{ width: "100%", color: "#999999", textDecoration: "line-through" }}>
          R$ {priceHigh}
        </span>

        <strong style={{ width: "100%", color: "#1c6f41", fontSize: "24px", marginTop: "5px" }}>
          R$ {priceLow}
        </strong>

        <small>
          {paymentType}
        </small>

      </div>
    )
  }
}


export default Card;
