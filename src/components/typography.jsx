import React, { PureComponent } from 'react';
import styled from 'styled-components';

import { color } from "./variables";


export const H2 = styled.h2`
    color: ${color.primary};
`;


export const Strong = styled.strong`
    ${props => props.primary && `
      color: ${color.primary};
    `}
    ${props => props.inverted && `
      color: ${color.inverted};
    `}
`;
