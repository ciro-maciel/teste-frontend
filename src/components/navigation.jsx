import React, { Component } from 'react';
import styled from 'styled-components';

import { color } from "./variables";
import { Strong } from "./typography.jsx";


const Container = styled.div`
  width: 100%;
`;

const ContainerMenu = styled.div`
  width: inherit;
  height: 60px;
  display: flex;
  align-items: center;
  background-color: #1c6f41;
  justify-content: center;
  box-shadow: rgba(90, 88, 88, 0.83) 0px 5px 6px;
  position: absolute;
`;

const ContainerMessage = styled.div`
  width: inherit;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #dddddd;
  margin-top: 60px
`;

const ListMenu = styled.ul`
  list-style-type: none;
  display: flex;
  justifyContent: space-between;
  width: 1000px;
  boxSizing: border-box;
  padding: 0;
  margin: 0;
  height: 60px;
`;

const ListMenuItem = styled.li`
  display: flex;
  flex: 1;
  align-items: center;
  padding: 0 20px;
  cursor: pointer;
  ${props => props.selected && `
    background-color: ${color.selected};
  `}

`;

const ListMenuSpacer = styled.li`
  border-right: 1px solid ${color.selected};
`;

const ImageMenu = styled.i`
  font-size: 30px;
  color: white;
  margin-right: 10px
`;



class Navigation extends Component {
  render() {
    return (
      <Container>
        <ContainerMenu>
          <ListMenu>
            <ListMenuItem>
              <ImageMenu className="fas fa-bars" />
              <Strong inverted>
                Nome do Menu
              </Strong>
            </ListMenuItem>
            <ListMenuSpacer />
            <ListMenuItem selected>
              <ImageMenu className="fas fa-cloud" />
              <Strong inverted>
                Nome do Menu
              </Strong>
            </ListMenuItem>
            <ListMenuSpacer />
            <ListMenuItem>
              <ImageMenu className="fab fa-android" style={{ fontSize: "40px" }} />
              <Strong inverted>
                Nome do Menu
              </Strong>
            </ListMenuItem>
            <ListMenuSpacer />
            <ListMenuItem>
              <ImageMenu className="far fa-file-alt" />
              <Strong inverted>
                Nome do Menu
              </Strong>
            </ListMenuItem>
            <ListMenuSpacer />
            <ListMenuItem>
              <ImageMenu className="fas fa-desktop" />
              <Strong inverted>
                Nome do Menu
              </Strong>
            </ListMenuItem>
          </ListMenu>
        </ContainerMenu>
        <ContainerMessage>
          Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos
        </ContainerMessage>
      </Container>
    )
  }
}


export default Navigation;
