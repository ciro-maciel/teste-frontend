import React, { Component } from 'react';
import styled from 'styled-components';

import Button from "./button.jsx";
import { Strong } from "./typography.jsx";


const Container = styled.div`
  height: 55px;
  display: flex;
  align-items: center;
  padding: 10px;
  justify-content: space-between;
`;

class Action extends Component {
  render() {
    return (
      <Container>
        <div>
          <Strong primary>
            3
          </Strong>
          &nbsp;
          <span>
            Itens econtrados
          </span>
        </div>
        <Button>
          Comparar
        </Button>
      </Container>
    )
  }
}


export default Action;
