import React, { Component } from 'react';



class Product extends Component {

  render() {

    const { category, product } = this.props.match.params;

    return (
      <div>
        <div>category - {category}</div>
        <div>product - {product}</div>
      </div>
    )
  }
}


export default Product;
