import React, { Component } from 'react';



class Category extends Component {

  render() {

    const { category } = this.props.match.params;

    return (
      <div>category - {category}</div>
      )
  }
}


export default Category;
