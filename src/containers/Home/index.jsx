import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Flex, Box } from 'grid-styled';

import Header from "../../components/header.jsx";
import Navigation from "../../components/navigation.jsx";
import Breadcrumb from "../../components/breadcrumb.jsx";
import Sidebar from "../../components/sidebar.jsx";
import Action from "../../components/action.jsx";
import Carrossel from "../../components/carrossel.jsx";
import Card from "../../components/card.jsx";
import Link from "../../components/link.jsx";
import { H2 } from "../../components/typography.jsx";


class Home extends Component {

  render() {

    const sideBarData = this.props.structure.sideBar.data;

    return (
      <Flex flexWrap='wrap' justify='center'>
        <Flex flexWrap='wrap' width={1000}>
          <Header />
        </Flex>
        <Navigation />
        <Flex flexWrap='wrap' width={1000}>
          <Breadcrumb />
          <Box p={2} width={[1, 3 / 12]}>
            <Sidebar data={sideBarData} />
          </Box>
          <Box width={[1, 9 / 12]}>
            <Action />
            <div style={{ textAlign: "center" }}>
              <H2>
                Televisores
              </H2>
            </div>
            <Carrossel>
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                // info="Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos"
                info="produto 1 produto 1 produto 1 produto 1"
                priceHigh="1.501,99"
                priceLow="1.301,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 2 produto 2 produto 2 produto 2"
                priceHigh="1.502,99"
                priceLow="1.302,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 3 produto 3 produto 3 produto 3"
                priceHigh="1.503,99"
                priceLow="1.303,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 4 produto 4 produto 4 produto 4"
                priceHigh="1.504,99"
                priceLow="1.304,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 5 produto 5 produto 5 produto 5"
                priceHigh="1.505,99"
                priceLow="1.305,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 6 produto 6 produto 6 produto 6"
                priceHigh="1.506,99"
                priceLow="1.306,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 7 produto 7 produto 7 produto 7"
                priceHigh="1.507,99"
                priceLow="1.307,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 8 produto 8 produto 8 produto 8"
                priceHigh="1.508,99"
                priceLow="1.308,99"
                paymentType="10x de R$ 999,99 sem juros" />
              <Card
                pictureSrc="../assets/img/products/product-televisor.png"
                info="produto 9 produto 9 produto 9 produto 9"
                priceHigh="1.509,99"
                priceLow="1.309,99"
                paymentType="10x de R$ 999,99 sem juros" />
            </Carrossel>
            <div style={{ textAlign: "center" }}>
              <Link href="#">
                ver mais
              </Link>
            </div>
          </Box>
        </Flex>
      </Flex>
    )
  }
}


export default connect(
  state => ({ structure: state.structure }),
)(Home);
