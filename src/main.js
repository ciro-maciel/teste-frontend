import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { BrowserRouter, HashRouter } from 'react-router-dom';

import { injectGlobal } from 'styled-components';

import store from './store';
import Routes from "./routes";

import { sideBarRequest } from './structure/actions';


injectGlobal`
	html,
	body,
	#container {
		width: 100%;
		height: 100%;
		padding: 0;
		margin: 0;
	}
	html {
		color: #666665;
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 14px;
  }

  div {
    box-sizing: border-box;
  }
`;

store.dispatch(sideBarRequest());

// https://github.com/facebookincubator/create-react-app/issues/1765
// https://reacttraining.com/react-router/web/api/HashRouter
render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </Provider>,
  document.getElementById('container'));
