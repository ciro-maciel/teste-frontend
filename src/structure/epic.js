import { SIDEBAR_REQUEST, SIDEBAR_RECEIVE } from './types';

import { sideBarReceive, sideBarFailure } from './actions';

import { Observable } from 'rxjs/Rx';


const ajax = (query) => Observable.ajax.get(`/assets/data/side_bar.json`);


export const sideBar = function (action$, store) {
  return Observable.merge(
    action$.ofType(SIDEBAR_REQUEST)
      // .do(action => console.log(action))
      // .do(action => console.log(store.getState()))
      // .delay(1000)
      .switchMap(({
        payload
      }) =>
        ajax(payload)
          .map(({
            response
          }) => sideBarReceive(response))
          .catch(({
            status
          }) => Observable.of(sideBarFailure(status)))
      )
  );
}
