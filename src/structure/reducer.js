import { SIDEBAR_REQUEST, SIDEBAR_RECEIVE, SIDEBAR_FAILURE } from './types';

let initialState = {
  data: null,
  isRequesting: false,
  isError: false,
  errorMessage: ''
}

export default function loadSideBar(state = initialState, action) {
  switch (action.type) {
    case SIDEBAR_REQUEST:
      return {
        ...state,
        isRequesting: true,
        isError: false,
        errorMessage: ''
      };
    case SIDEBAR_RECEIVE:
      return {
        ...state,
        data: action.payload,
        isRequesting: false
      };
    case SIDEBAR_FAILURE:
      return {
        ...state,
        isRequesting: false,
        isError: true,
        errorMessage: action.payload
      };
    default:
      return state;
  }
}
