import {
	SIDEBAR_REQUEST,
	SIDEBAR_RECEIVE,
	SIDEBAR_FAILURE
} from './types';

export function sideBarRequest() {
	return {
		type: SIDEBAR_REQUEST
	}
}

export function sideBarReceive(data) {
	return {
		type: SIDEBAR_RECEIVE,
		payload: data
	}
}

export function sideBarFailure(errorMessage) {
	return {
		type: SIDEBAR_FAILURE,
		payload: errorMessage,
		error: true
	}
}
