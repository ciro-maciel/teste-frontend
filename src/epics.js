import { combineEpics } from 'redux-observable';

import { sideBar } from './structure/epic';

export default combineEpics(
  sideBar
)
