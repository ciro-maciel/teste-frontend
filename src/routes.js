import React from 'react';
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom';

import { Flex, Box } from 'grid-styled';
import XRay from 'react-x-ray'

import Home from './containers/Home/index.jsx';
import Category from "./containers/Category";
import Product from "./containers/Product";


// https://reacttraining.com/react-router/web/guides/server-rendering/404-401-or-any-other-status
const Status = ({ code, children }) => (
  <Route render={({ staticContext }) => {
    if (staticContext)
      staticContext.status = code
    return children
  }} />
)

const NotFound = () => (
  <Status code={404}>
    <Flex justify='center' style={{ height: '100%' }}>
      <Flex width={1024}>
        <Box width={1} style={{ textAlign: 'center' }}>
          <h1>Sorry, can’t find that.</h1>
          <p>Go to <Link to={"/"}>Home</Link></p>
        </Box>
      </Flex>
    </Flex>
  </Status>
)

const RoutesSwitch = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    {/* <Route exact path="/users" component={Users} /> */}
    {/* <Route exact path="/:category" component={Category} /> */}
    <Route path="/:category/:product?" render={(props) => (
      (["001", "002", "003"].indexOf(props.match.params.category) != -1 && (["004", "005", "006"].indexOf(props.match.params.product) != -1)) ? (
        <Product {...props} />
      ) : (
          (["001", "002", "003"].indexOf(props.match.params.category) != -1) ? (
            <Category {...props} />
          ) : (
              <NotFound />
            )
        )
    )} />
    <Route component={NotFound} />
  </Switch>
)


const Routes = () => (
  <div>
    {
      (process.env.NODE_ENV && process.env.NODE_ENV !== 'production') ?
        <XRay disabled={true} color={'#e5a87d'} backgroundColor={'#004d84'} style={{ height: '100%' }} >
          <RoutesSwitch />
        </XRay>
        :
        <RoutesSwitch />
    }
  </div>
)

export default Routes;
